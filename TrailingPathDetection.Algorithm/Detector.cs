﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TrailingPathDetection.Algorithm
{
    public class InputFile
    {
        private string[] _segments;

        public string Path { get; set; }
        public string Name { get; set; }

        public string[] Segments => _segments ??= GetPathSegments();,

        public static char Separator => '.';

        private string[] GetPathSegments()
        {
            return Path.Split(Separator, StringSplitOptions.RemoveEmptyEntries);
        }

        public bool IsRoot() => Segments.Length == 1;

        public InputFile(string path, string name)
        {
            Path = path;
            Name = name;
        }
    }

    public class Detector
    {
        public HashSet<string> GetTrailing(InputFile[] files)
        {
            var ordered = files
                .OrderBy(x => x.Path)
                .ToList();

            var set = new HashSet<string>();

            foreach (var current in ordered)
            {
                if (current.IsRoot())
                {
                    set.Add(current.Path);
                    continue;
                }

                var parentPath = string.Join('.',current.Segments.Take(current.Segments.Length - 1));

                if (set.Contains(parentPath))
                {
                    set.Add(current.Path);
                }
            }

            var trailing = new HashSet<string>(files.Select(x => x.Path));

            trailing.ExceptWith(set);

            return trailing;
        }
    }
}
