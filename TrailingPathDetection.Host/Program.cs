﻿using System;
using TrailingPathDetection.Algorithm;

namespace TrailingPathDetection.Host
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            var detector = new Detector();

            var input = new InputFile[]
            {
                new InputFile("2", "ParentFolder"),
                new InputFile("2.4", "Files"),
                new InputFile("2.7", "Folder17"),
                new InputFile("4.2", "Folder18"),
                new InputFile("4.3.37", "Folder1722"),
                new InputFile("6.7", "Folder1722"),
                new InputFile("6", "Folder1722"),
                new InputFile("6.8.9", "Folder1722"),
            };

            var actual = detector.GetTrailing(input);
        }
    }
}
