﻿using System;
using System.Collections.Generic;
using System.Linq;
using TrailingPathDetection.Algorithm;
using Xunit;

namespace Tests
{
    public class TrailingFilesTests
    {
        [Theory]
        [MemberData(nameof(Input))]
        public void FindTrailingTest(InputFile[] input, InputFile[] expected)
        {
            var detector = new Detector();

            var actual = detector.GetTrailing(input);

            Assert.Equal(
                expected.Select(x => x.Path).OrderBy(x => x),
                actual.OrderBy(x => x));
        }

        public static IEnumerable<object[]> Input => new List<object[]>
        {
            new object[]
            {
                new []
                {
                    new InputFile("2", "ParentFolder"),
                    new InputFile("2.4", "Files"),
                    new InputFile("2.7", "Folder17"),
                    new InputFile("4.2", "Folder18"),
                    new InputFile("4.3.37", "Folder1722"),
                    new InputFile("6.8.0", "680"),
                    new InputFile("6", "Folder1722"),
                    new InputFile("6.8", "Folder1722"),
                    new InputFile("7.2", "Folder1722"),
                },
                new []
                {
                    new InputFile("4.2", "Folder18"),
                    new InputFile("4.3.37", "Folder1722"),
                    //new InputFile("6.8.0", "680"),
                    new InputFile("7.2", "72"),
                }
            }
        };
    }
}
